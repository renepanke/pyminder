from python_ini.ini_file import IniFile
from ReminderJob import ReminderJob
import sys

class Cfg:

    def __init__(self, file_path):
        self.__file_path = file_path
        self.ini = IniFile()
        self.ini.parse(self.__file_path)
        if (self.ini.errors):
            print("INI FILE ERRORS")
            print(self.ini.display_errors)
            sys.exit(1)
        self.jobs = self.get_jobs(self.ini.get_sections())

    def get_jobs(self, sections):
        jobs = []
        for section in sections:
            print(f"Reading section {section}")
            caption = self.ini.get_section_values(section, "caption")
            print(f"{section}->caption: {caption}")
            text = self.ini.get_section_values(section, "text")
            print(f"{section}->text: {text}")
            seconds = self.ini.get_section_values(section, "seconds")
            print(f"{section}->seconds: {seconds}")

            jobs.append(ReminderJob(caption, text, seconds))
        return jobs