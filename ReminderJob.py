import datetime
from windows_toasts import WindowsToaster, ToastText2

class ReminderJob:
    def __init__(self, caption, text, seconds):
        self.caption = caption
        self.text = text
        self.seconds = seconds

    def notify(self):
        print("Running ReminderJob at " + str(datetime.datetime.now()))
        wintoaster = WindowsToaster("Python")
        newToast = ToastText2()
        newToast.SetHeadline(self.caption)
        newToast.SetBody(self.text)
        wintoaster.show_toast(newToast)