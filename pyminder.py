import schedule
import time
import os
import sys
from ReminderJob import ReminderJob
from Cfg import Cfg
import time

def main():
    # Read config
    config = get_config()
    # Create jobs
    for job in config.jobs:
        schedule.every(job.seconds).seconds.do(job.notify)

    while True:
        schedule.run_pending()
        time.sleep(1)        

def get_config():
    home_dir = os.path.expanduser("~")
    config_dir = os.path.join(home_dir, ".pyminder")
    if not os.path.exists(config_dir):
        print("Configuration directory \"" + config_dir + "\" doesn't exist! Please create it.")
        sys.exit(1)
    config_file = os.path.join(config_dir, "config.ini")
    if not os.path.isfile(config_file):
        print("Configuration file \"" + config_file + "\" doesn't exist! Please create it.")
        sys.exit(1)
    
    return Cfg(config_file)


if __name__ == "__main__":
    main()