# pyminder

`pyminder` is a simple (windows only) tool to create chronologic reminders for yourself. Set the interval in seconds and get notified.

## Requirements

- `pip install schedule`
- `pip install IniFile`
- `pip install WindowsToaster`

## Configuration

The configuration takes place in `%USERPROFILE/.pyminder/config.ini`. Example:

```ini
[Standing_up]
caption=STAND UP!
text=Get some movement..
seconds=1800

[Look_far_away]
caption=Look far away
text=to reduce eye straing..
seconds=600

[Stay_hydrated]
caption=Ding something
text=Stay hydrated
seconds=1200
```

## How to run?

`python3 pyminder.py`
